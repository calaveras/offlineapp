# README #

This app demonstrates how to perform the offline tasks while connectivity becomes available.

To build, use gradle or import to Android Studio.

### How to test ###
* Launch the app
* The main window shows the activity log. You can reload or clear the log through menu.
* Create your condition and check the activity log. Search for "Perform pending tasks" for jobs 
that are performed.

### How it works ###
Two different mechanisms are used to trigger the pending tasks.
* CONNECTIVITY_ACTION : This is used to listen to connectivity events when the app is running. This event is triggered without any delay.
* JobScheduler: This is used to schedule jobs that will be triggered even when the app is not running.
The job is also scheduled to require network connectivity. This job often times comes with delay. 
The exact timing is controlled by the OS. Currently this job is scheduled to repeat every 5 min when there's 
network connectivity.

### Device reboot ###
The scheduling is designed to survive device reboot and app update. 

