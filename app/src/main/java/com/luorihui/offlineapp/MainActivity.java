package com.luorihui.offlineapp;

import android.app.Application;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.FileAppender;

public class MainActivity extends AppCompatActivity {
    Logger logger = LoggerFactory.getLogger("MainActivity");
    private static final String TAG = "RoyLuo";
    private TextView textView;
    private BroadcastReceiver connectivityReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logger.info("onCreate");
        MyJobService.scheduleJob(this.getApplication());
        connectivityReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
                int networkType = intent.getExtras().getInt(ConnectivityManager.EXTRA_NETWORK_TYPE);
                boolean isWiFi = networkType == ConnectivityManager.TYPE_WIFI;
                boolean isMobile = networkType == ConnectivityManager.TYPE_MOBILE;
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                logger.info(String.format("Connectivity action, isWifi:%s, isMobile:%s", isWiFi, isMobile));
                if (networkInfo != null) {
                    logger.info(String.format("type:%d, typeName:%s, isAvailable:%s, isConnected:%s, isConnectedOrConnecting:%s",
                            networkInfo.getType(),
                            networkInfo.getTypeName(),
                            networkInfo.isAvailable(),
                            networkInfo.isConnected(),
                            networkInfo.isConnectedOrConnecting()));
                    if (networkInfo.isConnected()) {
                        MyJobService.performPendingTasks();
                    }
                } else {
                    logger.info("No networkInfo");
                }
            }
        };
        registerReceiver(connectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        try {
            readLogFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        textView = (TextView)findViewById(R.id.mainView);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadLogFile();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        logger.info("onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        logger.info("onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (connectivityReceiver != null) {
            unregisterReceiver(connectivityReceiver);
        }
        logger.info("onDestroy");
    }

    private String readLogFile() throws IOException {
        FileInputStream is;
        BufferedReader reader;
        final File file = getLogFile();

        StringBuffer sb = new StringBuffer();
        if (file.exists()) {
            is = new FileInputStream(file);
            reader = new BufferedReader(new InputStreamReader(is));
            String line = reader.readLine();
            while(line != null){
                Log.d(TAG, line);
                sb.append(line + "\n");
                line = reader.readLine();
            }
            // Lazy close. Need better code for production
            reader.close();
        } else {
            Log.d(TAG, "Log file not exist:" + file.getAbsolutePath());
        }
        return sb.toString();
    }

    @NonNull
    private File getLogFile() {
        return new File(getExternalFilesDir(null), "offlineapp.log");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_reload) {
            loadLogFile();
            return true;
        }

        if (id == R.id.action_clear) {
            clearLogFile();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void clearLogFile() {
        getLogFile().delete();
        try {
            getLogFile().createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        loadLogFile();
    }

    private void loadLogFile() {
        try {
            textView.setText(readLogFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
