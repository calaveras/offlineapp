package com.luorihui.offlineapp;

import android.app.Application;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.os.PersistableBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by luorihui on 4/14/17.
 */

public class MyJobService extends JobService {
    private final static int JOB_ID = 1;
    private static final Logger logger = LoggerFactory.getLogger("MyJobService");
    @Override
    public boolean onStartJob(JobParameters jobParameters) {
        logger.info(String.format("onStartJob:jobId:%d", jobParameters.getJobId()));
        MyJobService.performPendingTasks();
        jobFinished(jobParameters, false);
        return false; // Tell the OS the job has done. So onStopJob won't be called.
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        logger.info("onStopJob:" + jobParameters.toString());
        return true;
    }

    public static void scheduleJob(Context context) {
        JobScheduler jobScheduler = (JobScheduler)context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
//        if (isJobPending(jobScheduler)) {
//            logger.info("Job pending, do nothing");
//            return;
//        }

        ComponentName mServiceComponent = new ComponentName(context, MyJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, mServiceComponent);
        builder.setPeriodic(5*60*1000); // Every 5 min
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        int result = jobScheduler.schedule(builder.build());
        logger.info("scheduleJob, result:" + result);
    }

    private static boolean isJobPending(JobScheduler jobScheduler) {
        List<JobInfo> jobs = jobScheduler.getAllPendingJobs();
        boolean found = false;
        for (JobInfo j : jobs) {
            if (j.getId() == JOB_ID) {
                found = true;
                break;
            }
        }
        return  found;
    }

    public static void performPendingTasks() {
        // This is the place that perform all the pending tasks.
        // Make sure it's not run in main thread
        logger.info("Perform pending tasks");
    }

}
