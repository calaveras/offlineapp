package com.luorihui.offlineapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by luorihui on 4/14/17.
 */

public class StartUpBootReceiver extends BroadcastReceiver {
    private static final Logger logger = LoggerFactory.getLogger("StartUpBootReceiver");

    @Override
    public void onReceive(Context context, Intent intent) {
        logger.info("onReceive");
        MyJobService.scheduleJob(context);
    }
}
