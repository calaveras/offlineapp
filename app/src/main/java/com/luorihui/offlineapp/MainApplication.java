package com.luorihui.offlineapp;

import android.app.Application;
import android.os.StrictMode;

/**
 * Created by luorihui on 4/17/17.
 */

public class MainApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
    }
}
